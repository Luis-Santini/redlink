package com.santini.redlink.model

import com.google.gson.annotations.SerializedName

data class Albums(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("userId")
    val userId: Int
)

data class Photos(
    @SerializedName("albumId")
val albumId: Int,
@SerializedName("id")
val id: Int,
@SerializedName("thumbnailUrl")
val thumbnailUrl: String,
@SerializedName("title")
val title: String,
@SerializedName("url")
val url: String
    )