package com.santini.redlink.model

import com.google.gson.annotations.SerializedName


//class AlbumPhotos : ArrayList<Albums>()

data class AlbumsNetworkDto(
    @SerializedName("dataAlbums")
    val dataAlbums: List<Albums>,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean

)


data class PhotosNetworkDto(
    @SerializedName("dataPhotos")
    val dataPhotos: List<Photos>,
)