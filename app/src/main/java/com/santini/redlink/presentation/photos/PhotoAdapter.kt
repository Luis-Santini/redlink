package com.santini.redlink.presentation.photos

import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.santini.redlink.R
import com.santini.redlink.databinding.ItemsPhotosBinding
import com.santini.redlink.model.Albums
import com.santini.redlink.model.Photos
import com.squareup.picasso.Picasso

class PhotoAdapter
    (var photos: List<Photos> = listOf(),
     var callbackItemAlbum : (Photos)->Unit) :
    RecyclerView.Adapter<PhotoAdapter.PhotosAdapterViewHolder>() {


    inner class PhotosAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemsPhotosBinding = ItemsPhotosBinding.bind(itemView)

        fun bind(photo: Photos)=
        with(binding){
            Picasso.get().load(photo.url).into(imgPhoto)
            tvIdPhoto.setText(photo.id.toString())
            binding.tvTitlePhotos.setText(photo.title)
        }

    }

   fun updateListaPhotos(photos: List<Photos>) {
        this.photos = photos
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosAdapterViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.items_photos, parent, false)
        return PhotosAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhotosAdapterViewHolder, position: Int) {
        val photos = photos[position]
        holder.bind(photos)

    }

    override fun getItemCount(): Int {
        return photos.size
    }

}
