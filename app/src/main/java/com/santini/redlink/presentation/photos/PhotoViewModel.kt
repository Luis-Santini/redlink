package com.santini.redlink.presentation.photos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.redlink.data.Api
import com.santini.redlink.model.Photos
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PhotoViewModel:ViewModel() {

    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _photos = MutableLiveData<List<Photos>>()
    val photos: LiveData<List<Photos>> = _photos

    fun obtenerPhotos(uuiAlbum:String){
    viewModelScope.launch {
        _loader.value = true
        try {
            val resultado = withContext(Dispatchers.IO){
                Api.build().obtenerPhotosFiltrado(uuiAlbum)
            }
            if (resultado.isSuccessful) {
                _photos.value = resultado.body()
            } else {
                _error.value = resultado.message()
            }

        }catch (e: Exception){

            _error.value = e.message

        }finally {
            _loader.value = false
        }
    }
    }


}