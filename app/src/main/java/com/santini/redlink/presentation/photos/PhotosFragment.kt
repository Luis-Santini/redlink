package com.santini.redlink.presentation.photos

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaopiz.kprogresshud.KProgressHUD
import com.santini.redlink.R
import com.santini.redlink.databinding.FragmentPhotosBinding
import com.santini.redlink.presentation.albums.AlbumsAdapter
import com.santini.redlink.presentation.albums.AlbumsFragmentDirections


class PhotosFragment : Fragment() {
    private var uuiAlbums: String = ""


    private val adapter : PhotoAdapter by lazy {


        PhotoAdapter() {

        }



    }

    private lateinit var binding: FragmentPhotosBinding
    private val viewModel: PhotoViewModel by viewModels()

    private lateinit var progress: KProgressHUD
    private lateinit var globalView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_photos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        globalView = view
        binding = FragmentPhotosBinding.bind(view)

        getArgs()
        setUpAdapter()
        loadData()
        setupObservables()
    }

    private fun loadData() {
        viewModel.obtenerPhotos(uuiAlbums)

    }
    private fun setUpAdapter() {
        binding.rvphotos.adapter = adapter
     //   binding.rvphotos.layoutManager = GridLayoutManager(requireContext(),1)
        binding.rvphotos.layoutManager = LinearLayoutManager(requireContext()
        )

    }
    private fun getArgs() {
        arguments?.let {
            uuiAlbums = PhotosFragmentArgs.fromBundle(it).uuiAlbum
            Toast.makeText(requireContext(), uuiAlbums, Toast.LENGTH_SHORT).show()
        }
    }
    private fun setupObservables() {
        viewModel.loader.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                progress = KProgressHUD.create(requireContext())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por Favor, espere")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show()
            } else {
                progress.dismiss()
            }
        })
        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })
        viewModel.photos.observe(viewLifecycleOwner, Observer {
            //RECIBO LA LISTA Y DEBERIA PASARSELO AL ADAPTER PARA QUE LO PINTE EN MI LISTA
            // adapter.updateLista(it)
          adapter.updateListaPhotos(it)
        })
    }
}