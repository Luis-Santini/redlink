package com.santini.redlink.presentation.albums

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.santini.redlink.R
import com.santini.redlink.databinding.ItemsAlbumsBinding
import com.santini.redlink.model.Albums
import java.util.*
import kotlin.collections.ArrayList

class AlbumsAdapter (var albums: ArrayList<Albums> = ArrayList(),
                     var callbackItemAlbum : (Albums)->Unit) :
    RecyclerView.Adapter<AlbumsAdapter.AlbumsAdapterViewHolder>(), Filterable {
    var albumsSinFiltrar = ArrayList<Albums>()
    var albumsFilterList = ArrayList<Albums>()
    inner class AlbumsAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding: ItemsAlbumsBinding = ItemsAlbumsBinding.bind(itemView)
        fun bind(albums: Albums) {
            binding.tvIdAlbums.setText(albums.id.toString())
            binding.tvTitleAlbums.setText(albums.title)
            binding.root.setOnClickListener {
                callbackItemAlbum(albums)
            }
        }
    }
    init {
        albumsFilterList = albums
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsAdapterViewHolder {
        val view =  LayoutInflater.from(parent.context).inflate(R.layout.items_albums, parent, false)
        return AlbumsAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlbumsAdapterViewHolder, position: Int) {
        val albums  = albums[position]
        holder.bind(albums)
    }
    override fun getItemCount(): Int {
       return albums.size
    }
    fun updateLista( albums: ArrayList<Albums> ) {
        this.albums = albums
        albumsSinFiltrar =albums
        notifyDataSetChanged()
    }
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                albumsFilterList.clear()
                if (charSearch.isEmpty()) {
                    albumsFilterList.addAll(albumsSinFiltrar)
                } else {
                    val resultList = ArrayList<Albums>()
                    for (row in albumsSinFiltrar) {
                       if (row.title.lowercase(Locale.ROOT).contains(charSearch.lowercase(Locale.ROOT))) {
                           resultList.add(row)
                      }
                    }
                   albumsFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values =  albumsFilterList
                return filterResults
            }
            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                albums = results?.values as ArrayList<Albums>
                notifyDataSetChanged()
            }

        }

    }



}