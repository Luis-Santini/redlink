package com.santini.redlink.presentation.albums

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaopiz.kprogresshud.KProgressHUD
import com.santini.redlink.R
import com.santini.redlink.databinding.FragmentAlbumsBinding

 class AlbumsFragment : Fragment(), SearchView.OnQueryTextListener {
    private lateinit var binding: FragmentAlbumsBinding
    private val viewModel: AlbumsViewModel by viewModels()
    private lateinit var progress: KProgressHUD
    private lateinit var globalView: View

    private val adapter: AlbumsAdapter by lazy {
        AlbumsAdapter() {
            val directions =
                AlbumsFragmentDirections.actionAlbumsFragmentToPhotosFragment(it.id.toString())
            Navigation.findNavController(globalView).navigate(directions)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_albums, container, false)



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentAlbumsBinding.bind(view)
        binding.searchAlbum.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)

                return false
            }
        })
        globalView = view

        loadData()
        setupObservables()
        setupAdapter()



    }

    private fun setupObservables() {
        viewModel.loader.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                progress = KProgressHUD.create(requireContext())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    .setLabel("Por Favor, espere")
                    .setCancellable(false)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f)
                    .show()
            } else {
                progress.dismiss()
            }
        })

        viewModel.error.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        viewModel.albumns.observe(viewLifecycleOwner, Observer {
            adapter.updateLista(it)
        })


    }


    private fun loadData() {
        viewModel.obtenerAlbums()

    }

    private fun setupAdapter() {

        binding.rvAlbums.adapter = adapter
        binding.rvAlbums.layoutManager = LinearLayoutManager(requireContext())



    }
     override fun onQueryTextSubmit(query: String?): Boolean {
         return false
     }

     override fun onQueryTextChange(newText: String?): Boolean {
         adapter.filter.filter(newText)
         return false
     }

}



