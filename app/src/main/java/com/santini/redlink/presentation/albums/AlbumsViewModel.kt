package com.santini.redlink.presentation.albums

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.redlink.data.Api
import com.santini.redlink.model.Albums
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AlbumsViewModel : ViewModel() {
    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean> = _loader

    private val _error = MutableLiveData<String>()
    val error: LiveData<String> = _error

    private val _albumns = MutableLiveData<ArrayList<Albums>>()
    val albumns: LiveData<ArrayList<Albums>> = _albumns

    fun obtenerAlbums() {

        viewModelScope.launch {
            _loader.value = true

           try {
                val resultado = withContext(Dispatchers.IO) {
                    Api.build().obtenerTodosAlbums()
                }
                if (resultado.isSuccessful) {
                    _albumns.value = resultado.body()
                } else {
                    _error.value = resultado.message()
                }
            } catch (e: Exception) {
               _error.value = e.message.toString()
           } finally {
               _loader.value = false
           }
        }

    }




}