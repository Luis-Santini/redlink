package com.santini.redlink.data

import androidx.core.content.res.FontResourcesParserCompat
import com.santini.redlink.model.*
import com.santini.redlink.util.Constantes
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


object Api {

    private val builder: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(Constantes.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())


    interface ApiInterface {
        @GET("albums")
        suspend fun obtenerTodosAlbums(): Response<ArrayList<Albums>>

        @GET("albums/{id}")// consulta get con path
        suspend fun obtenerAlbumFiltrado(
            @Path("id") id: Int
        ): Response<AlbumsNetworkDto>


        @GET("photos")// consulta get con path
        suspend fun obtenerPhotosFiltrado(
            @Query("albumId") albumId: String
        ): Response<List<Photos>>


    }

    fun build(): ApiInterface {
        val apiInterface = builder.build().create(ApiInterface::class.java)
        return apiInterface
    }

}